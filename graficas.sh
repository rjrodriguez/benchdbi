#!/bin/bash
#Generación de gráficas para los datos procesados

. ./benchDBI.cfg

#(Instrumentación por instrucciones y por bloques básicos)
#-Tiempo de ejecución de las aplicaciones
#-Consumo de memoria por aplicaciones
#-Numero de instrucciones ejecutadas por aplicaciones
#-Slowdown de media

#Ficheros temporales
FMEM=/tmp/fmem.$$
FSLO=/tmp/fslo.$$

if [ $# -ne 1 ] ;
then
	echo "Uso"
	echo "$0: <fichero.salida.csv>"
	exit 1
fi

FCSV=$1

if [ ! -f $FCSV ] ;
then
	echo "$0: $FCSV no existe o no accesible"
	exit 1
fi

echo "Inicio del proceso del fichero $FCSV"

#1 Consumo de memoria de las aplicaciones

#Separar por dbi
#Separar por aplicacion
#No separo por instrumentación
#No separo por optimización
for dbi in $FR_DBI
do
	for apl in $PRUEBAS_BENCH
	do
		cat $FCSV | grep $dbi | grep $apl| cut -d ";" -f 11 >> $FMEM.$apl.$dbi
		num=`cat $FMEM.$apl.$dbi | wc -l`
		lista=`cat $FMEM.$apl.$dbi`
		suma=`echo $lista | sed "s/ / + /g" `
		total=`expr $suma`
		media=`expr $total / $num `
		echo "$media $apl" >> $FMEM.$dbi.gnuplot		
		
	#de $PRUEBAS_BENCH
	done

#de $FR_DBI
done

#Ahora saco la parte sin instrumentar
for apl in $PRUEBAS_BENCH
do
	cat $FCSV | grep ";$apl"| cut -d ";" -f 11 >> $FMEM.$apl.none
	num=`cat $FMEM.$apl.none | wc -l`
	lista=`cat $FMEM.$apl.none`
	suma=`echo $lista | sed "s/ / + /g" `
	total=`expr $suma`
	media=`expr $total / $num `
	echo "$media $apl" >> $FMEM.none.gnuplot		
		
#de $PRUEBAS_BENCH
done

echo "set title \"Consumo de memoria\" ">  $FMEM.gnuplot
echo "set xlabel \"Aplicaciones\" " 	>> $FMEM.gnuplot
echo "set ylabel \"KiB\" " 		>> $FMEM.gnuplot
echo "plot \"$FMEM.drrun.gnuplot\" using 1:xticlabels(2) with lines title \"DynamoRio\", \"$FMEM.valgrind.gnuplot\" using 1:xticlabels(2) with lines title \"Valgrind\", \"$FMEM.pin.gnuplot\" using 1:xticlabels(2) with lines title \"Pin\", \"$FMEM.none.gnuplot\" using 1:xticlabels(2) with lines title \"Sin Instr\" " >> $FMEM.gnuplot
echo "pause -1 \"Pulse una tecla para continuar\" " >> $FMEM.gnuplot

gnuplot $FMEM.gnuplot

#2 Slowdown 
#Separar por dbi
#Separar por aplicacion
#Separar por instrumentación
#Separar por optimización ¡¡¡¡FALTA!!!!

cuenta=1	#Para los graficos de barras d
for dbi in $FR_DBI
do
	for optiz in $OPTIMIZACIONES 
	do
		elemento=0
		for apl in $PRUEBAS_BENCH
		do
	
			#Averiguo el tiempo que le cuesta ejecutarse a la aplicacion 
			cat $FCSV | grep ";$apl.$optiz"  | cut -d ";" -f 2 | cut -d "." -f 1 > $FSLO.$apl.$optiz
			num=`cat $FSLO.$apl.$optiz | wc -l`
			lista=`cat $FSLO.$apl.$optiz`
			suma=`echo $lista | sed "s/ / + /g" `
			total=`expr $suma`
			media=`expr $total / $num `
			media_apl=$media

			#Ahora busco el tiempo instrumentado y calcuclo el slowdown
			for instr in icuenta bbcuenta
			do
				cat $FCSV | grep $dbi | grep $apl.$optiz | grep $instr | cut -d ";" -f 2| cut -d "." -f 1 >> $FSLO.$apl.$optiz.$dbi.$instr
				num=`cat  $FSLO.$apl.$optiz.$dbi.$instr| wc -l`
				lista=`cat $FSLO.$apl.$optiz.$dbi.$instr`
				suma=`echo $lista | sed "s/ / + /g" `
				total=`expr $suma`
				media=`echo "scale=3 ; $total / $num " | bc -l`
				slow=`echo "scale=3 ; $media / $media_apl " | bc -l`
				posicion=`expr $elemento \* 4 + $cuenta`
				echo "$slow $posicion $apl" >> $FSLO.$dbi.$instr.$optiz.gnuplot
	
			#de "icuenta bbcuenta"
			done

			elemento=`expr $elemento + 1`		
		#de $PRUEBAS_BENCH
		done

	#de $OPTIMIZACIONES
	done

	cuenta=`expr $cuenta + 1`  	
#de $FR_DBI
done

echo "set title \"Slowdown por instrucciones Opt 0\" ">  $FSLO.gnuplot
echo "set xlabel \"Aplicaciones\" " 	>> $FSLO.gnuplot
echo "set ylabel \"Slowdown\" " 	>> $FSLO.gnuplot
echo "set style fill solid 0.25 border" >> $FSLO.gnuplot
echo "set boxwidth 0.75" 		>> $FSLO.gnuplot
echo "set yrange [0:20]" 		>> $FSLO.gnuplot
echo "plot \"$FSLO.drrun.icuenta.O0.gnuplot\" using 2:1:xticlabels(3) with boxes title \"DynamoRio\", \"$FSLO.pin.icuenta.O0.gnuplot\"  using 2:1 with boxes title \"PIN\", \"$FSLO.valgrind.icuenta.O0.gnuplot\" using 2:1 with boxes title \"Valgrind\" " >> $FSLO.gnuplot
echo "pause -1 \"Pulse una tecla para continuar\" " >> $FSLO.gnuplot
echo "set title \"Slowdown por instrucciones Opt 3\" ">>  $FSLO.gnuplot
echo "plot \"$FSLO.drrun.icuenta.O3.gnuplot\" using 2:1:xticlabels(3) with boxes title \"DynamoRio\", \"$FSLO.pin.icuenta.O3.gnuplot\"  using 2:1 with boxes title \"PIN\", \"$FSLO.valgrind.icuenta.O3.gnuplot\" using 2:1 with boxes title \"Valgrind\" " >> $FSLO.gnuplot
echo "pause -1 \"Pulse una tecla para continuar\" " >> $FSLO.gnuplot
echo "set title \"Slowdown por Bloques Opt 0\" ">>  $FSLO.gnuplot
echo "plot \"$FSLO.drrun.bbcuenta.O0.gnuplot\" using 2:1:xticlabels(3) with boxes title \"DynamoRio\", \"$FSLO.pin.bbcuenta.O0.gnuplot\"  using 2:1 with boxes title \"PIN\", \"$FSLO.valgrind.bbcuenta.O0.gnuplot\" using 2:1 with boxes title \"Valgrind\" " >> $FSLO.gnuplot
echo "pause -1 \"Pulse una tecla para continuar\" " >> $FSLO.gnuplot
echo "set title \"Slowdown por Bloques Opt 3\" ">>  $FSLO.gnuplot
echo "plot \"$FSLO.drrun.bbcuenta.O3.gnuplot\" using 2:1:xticlabels(3) with boxes title \"DynamoRio\", \"$FSLO.pin.bbcuenta.O3.gnuplot\"  using 2:1 with boxes title \"PIN\", \"$FSLO.valgrind.bbcuenta.O3.gnuplot\" using 2:1 with boxes title \"Valgrind\" " >> $FSLO.gnuplot
echo "pause -1 \"Pulse una tecla para continuar\" " >> $FSLO.gnuplot

gnuplot $FSLO.gnuplot
