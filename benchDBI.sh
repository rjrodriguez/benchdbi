#!/bin/bash

# Benchmark benchDBI

cd apps

. ../benchDBI.cfg

function monitoriza_proceso()
{
  procmon=`jobs -p 1`
  echo "monitorizamos al proceso hijo de $procmon"
  consumo=`$DIR_BENCHMARK/../monitoriza.sh $procmon  `
  echo "Consumo de  $consumo"
  jobs > /dev/null 2> /dev/null


}

if [ -f $SALIDA ] 
then
	echo "Ya existía fichero de salida de datos $SALIDA y se borra"
	rm $salida
fi

echo "Fichero de salida de datos: $SALIDA"

echo PATH es $PATH

echo "Lanzando benchmark "

echo "Procesando directorios"

for directorios in  $PRUEBAS_BENCH
do
   echo ; echo ; echo "BENCHMARK: $directorios"
   cd $DIR_BENCHMARK/$directorios
   echo "Directorio actual `pwd`"
   if [ -f $DIR_BENCHMARK/$directorios/benchDBI.dir.cfg ] ;
   then
     #Carga del fichero de configuracion del directorio
     . ./benchDBI.dir.cfg
     for numero_pruebas in $NUM_PRUEBAS
     do
       	echo "Ronda $numero_pruebas"
        for optimizaciones in $OPTIMIZACIONES
        do
	   if [ -f $EJECUTABLE.$optimizaciones ]
           then
	     echo "Probando optimizacion $optimizaciones"
	     #for fich_prueba in $FICH_PRUEBA
	     #do
	       echo "Ejecutando $EJECUTABLE.$optimizaciones $PARAM_PRE $FICH_PRUEBA $PARAM_POST"
               $EJEC_TIME $EJECUTABLE.$optimizaciones $PARAM_PRE $FICH_PRUEBA $PARAM_POST > /dev/null 2>> $LOG &
	       monitoriza_proceso
	       echo `cat $LOG | tail -1`";$consumo;0" >> $SALIDA 	

	       for instr in $FR_DBI
	       do
	         echo "Ejecutando con instrumentacion por instrucciones" 
	   	 case $instr in
		 pin)
			echo PIN
			$EJEC_TIME pin -t $DIR_BENCHMARK/DBAs/pin/icuenta.so --  $EJECUTABLE.$optimizaciones $PARAM_PRE $FICH_PRUEBA $PARAM_POST > /dev/null 2>> $LOG &
			;;
		 valgrind)
			echo VALGRIND
			$EJEC_TIME valgrind --tool=icuenta $EJECUTABLE.$optimizaciones $PARAM_PRE $FICH_PRUEBA $PARAM_POST > /dev/null 2>> $LOG &
			;;
		 drrun)
			echo DynamoRIO
			$EJEC_TIME drrun -root $ROOT_DR -client $DIR_BENCHMARK/DBAs/dynamorio/libicuenta.so 1 "" $EJECUTABLE.$optimizaciones $PARAM_PRE $FICH_PRUEBA $PARAM_POST > /dev/null 2>> $LOG &

			;;
		 esac  
		 monitoriza_proceso
		 case $instr in
                 pin)	echo `cat $LOG | tail -1`";$consumo;"`cat icuenta.out | cut -f 2 -d " "` >> $SALIDA
			;;
                 valgrind)
			echo `cat $LOG | tail -1`";$consumo;"`tail -2 $LOG | head -1 |  cut -f 3 -d " "` >> $SALIDA
 			;;
                 drrun)
			echo `cat $LOG | tail -1`";$consumo;"`tail -2 $LOG | head -1 |  cut -f 2 -d " "` >> $SALIDA
			;;
                 esac

		 echo "Ejecutando con instrumentacion por bloques básicos" 
	   	 case $instr in
		 pin)
			echo PIN
			$EJEC_TIME pin -t $DIR_BENCHMARK/DBAs/pin/bbcuenta.so --  $EJECUTABLE.$optimizaciones $PARAM_PRE $FICH_PRUEBA $PARAM_POST > /dev/null 2>> $LOG &
			;;
		 valgrind)
			echo VALGRIND
			$EJEC_TIME valgrind --tool=bbcuenta $EJECUTABLE.$optimizaciones $PARAM_PRE $FICH_PRUEBA $PARAM_POST > /dev/null 2>> $LOG &
			;;
		 drrun)
			echo DynamoRIO
			$EJEC_TIME drrun -root $ROOT_DR -client $DIR_BENCHMARK/DBAs/dynamorio/libbbcuenta.so 1 "" $EJECUTABLE.$optimizaciones $PARAM_PRE $FICH_PRUEBA $PARAM_POST > /dev/null 2>> $LOG &

			;;
		 esac  
		 monitoriza_proceso
		 case $instr in
                 pin)	echo `cat $LOG | tail -1`";$consumo;"`cat bbcuenta.out | cut -f 2 -d " "` >> $SALIDA
			;;
                 valgrind)
			echo `cat $LOG | tail -1`";$consumo;"`tail -2 $LOG | head -1 |  cut -f 3 -d " "` >> $SALIDA
 			;;
                 drrun)
			echo `cat $LOG | tail -1`";$consumo;"`tail -2 $LOG | head -1 |  cut -f 3 -d " "` >> $SALIDA
			;;
                 esac




	       done

	     #done #for fich_prueba
	   else
	     echo "No existe la optimizacion $optimizaciones"
           fi
        done #for optimizaciones

     done #for numero_pruebas
   else
     echo "Directorio sin fichero de configuracion"
   fi
done #for directorios 
