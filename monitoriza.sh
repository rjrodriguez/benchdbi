#Sin shell
#Tiempo en segundos para hacer la consulta
INTERVALO=1
#Consumo de memoria del proceso
MAXMEM=0

LOG=/tmp/$$.log

#Comprobaci�n de par�metros
if [ $# -ne 1 ] ;
then
    echo "$0: Hace falta un par�metro"
    exit
else
    proceso=$1
    #echo "todo ok con $proceso"
fi

#Esperamos a que se acabe de lanzar el proceso
sleep 1

#Hay que averiguar qui�n es el hijo del proceso $proceso
for hijos in $(ps axo pid,ppid  | \
   awk "{ if ( \$2 == $proceso ) { print \$1 }}")
do
   echo "$0: monitorizamos a $hijos" >> $LOG
   monitorizar=$hijos
done


while ( test -d /proc/$monitorizar ) ;
do
  VMPEAK=`cat /proc/$monitorizar/status | grep VmPeak | cut -c8-16`
  if [ $VMPEAK -gt $MAXMEM ] ;
  then
   MAXMEM=$VMPEAK
  fi
  sleep $INTERVALO
done
echo $MAXMEM
