/*
   Copyright (C) 2002-2012 Nicholas Nethercote
      njn@valgrind.org

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307, USA.

   The GNU General Public License is contained in the file COPYING.
*/

/* benchDBI */
/* Instrumentacion por bloques basicos */

#include "pub_tool_basics.h"
#include "pub_tool_tooliface.h"
#include "pub_tool_libcassert.h"
#include "pub_tool_libcprint.h"
#include "pub_tool_debuginfo.h"
#include "pub_tool_libcbase.h"
#include "pub_tool_options.h"
#include "pub_tool_machine.h"     


static Bool lk_process_cmd_line_option(Char* arg)
{
   return True;
}

static void lk_print_usage(void)
{  
   VG_(printf)("\n");
} 

static void lk_print_debug_usage(void)
{  
   VG_(printf)("\n");
}

/* Contador de bloques ejecutados */
static ULong n_SBs_entered   = 0;

/* Funcion que se añadira */
static void add_one_SB_entered(void)
{
   n_SBs_entered++;
}

static void lk_post_clo_init(void)
{
}


/* Funcion llamada por pin antes de ejecutar un super bloque */
static
IRSB* lk_instrument ( VgCallbackClosure* closure,
                      IRSB* sbIn, 
                      VexGuestLayout* layout, 
                      VexGuestExtents* vge,
                      IRType gWordTy, IRType hWordTy )
{
   IRDirty*   di;
   Int        i;
   IRSB*      sbOut;

   if (gWordTy != hWordTy) {
      VG_(tool_panic)("host/guest word size mismatch");
   }

   sbOut = deepCopyIRSBExceptStmts(sbIn);

   i = 0;
   while (i < sbIn->stmts_used && sbIn->stmts[i]->tag != Ist_IMark) {
      addStmtToIRSB( sbOut, sbIn->stmts[i] );
      i++;
   }
 
   //Entramos en un SuperBloque
      di = unsafeIRDirty_0_N( 0, "add_one_SB_entered", 
                                 VG_(fnptr_to_fnentry)( &add_one_SB_entered ),
                                 mkIRExprVec_0() );
      addStmtToIRSB( sbOut, IRStmt_Dirty(di) );

   //Se recorren las instrucciones
   for (; i < sbIn->stmts_used; i++) {
      	IRStmt* st = sbIn->stmts[i];
      	if (!st || st->tag == Ist_NoOp) continue;
	addStmtToIRSB( sbOut, st );
   }

   return sbOut;
}

static void lk_fini(Int exitcode)
{
 
      VG_(umsg)("superbloques: %llu\n", n_SBs_entered);


}

static void lk_pre_clo_init(void)
{
   VG_(details_name)            ("bbcuenta");
   VG_(details_version)         (NULL);
   VG_(details_description)     ("Cuenta de superbloques ");
   VG_(details_copyright_author)("Copyright (C) 2002-2012, and GNU GPL'd, by Nicholas Nethercote."); 
   VG_(details_bug_reports_to)  (VG_BUGS_TO);
   VG_(details_avg_translation_sizeB) ( 200 );

   VG_(basic_tool_funcs)          (lk_post_clo_init,
                                   lk_instrument,
                                   lk_fini);
   VG_(needs_command_line_options)(lk_process_cmd_line_option,
                                   lk_print_usage,
                                   lk_print_debug_usage);
}

VG_DETERMINE_INTERFACE_VERSION(lk_pre_clo_init)


