/*
   Copyright (C) 2002-2012 Nicholas Nethercote
      njn@valgrind.org

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307, USA.

   The GNU General Public License is contained in the file COPYING.
*/

/* benchDBI */
/* Instrumentacion por instrucciones */


#include "pub_tool_basics.h"
#include "pub_tool_tooliface.h"
#include "pub_tool_options.h"   
#include "pub_tool_libcbase.h"  
#include "pub_tool_libcassert.h" 
#include "pub_tool_machine.h"  
#include "pub_tool_libcprint.h"
#include "pub_tool_debuginfo.h"

static Bool ic_process_cmd_line_option(Char* arg)
{
   return True;
}

static void ic_print_usage(void)
{  
   VG_(printf)("    (none)\n");
}

/* Contador de instrucciones ejecutadas */
static ULong n_guest_instrs  = 0;

/* Funcion que se añadira */
static void add_one_guest_instr(void)
{
   n_guest_instrs++;
}

static void ic_post_clo_init(void)
{
}

static
IRSB* ic_instrument ( VgCallbackClosure* closure,
                      IRSB* sbIn,
                      VexGuestLayout* layout, 
                      VexGuestExtents* vge,
                      IRType gWordTy, IRType hWordTy )
{
   IRDirty*   di;
   Int        i;
   IRSB*      sbOut;
   IRTypeEnv* tyenv = sbIn->tyenv;

   if (gWordTy != hWordTy) {
      /* We don't currently support this case. */
      VG_(tool_panic)("host/guest word size mismatch");
   }

   /* Set up SB */
   sbOut = deepCopyIRSBExceptStmts(sbIn);

   // Copy verbatim any IR preamble preceding the first IMark
   i = 0;
   while (i < sbIn->stmts_used && sbIn->stmts[i]->tag != Ist_IMark) {
      addStmtToIRSB( sbOut, sbIn->stmts[i] );
      i++;
   }

   for (; i < sbIn->stmts_used; i++) {
      IRStmt* st = sbIn->stmts[i];
      if (!st || st->tag == Ist_NoOp) continue;

      switch (st->tag) {
 
         case Ist_IMark:
               /* Count guest instruction. */
               di = unsafeIRDirty_0_N( 0, "add_one_guest_instr",
                                          VG_(fnptr_to_fnentry)( &add_one_guest_instr ), 
                                          mkIRExprVec_0() );
               addStmtToIRSB( sbOut, IRStmt_Dirty(di) );

            addStmtToIRSB( sbOut, st );
            break;

         default: 
	    addStmtToIRSB( sbOut, st );      // Original statement
            break;
            
      }
   }
   return sbOut;

}

static void ic_fini(Int exitcode)
{

      VG_(umsg)("instrucciones %llu\n", n_guest_instrs);
 
}

static void ic_pre_clo_init(void)
{
   VG_(details_name)            ("icuenta");
   VG_(details_version)         (NULL);
   VG_(details_description)     ("Contador de instrucciones");
   VG_(details_copyright_author)("Copyright (C) 2002-2012, and GNU GPL'd, by Nicholas Nethercote."); 
   VG_(details_bug_reports_to)  (VG_BUGS_TO);

   VG_(details_avg_translation_sizeB) ( 275 );

   VG_(basic_tool_funcs)        (ic_post_clo_init,
                                 ic_instrument,
                                 ic_fini);
   VG_(needs_command_line_options)(ic_process_cmd_line_option,
                                   ic_print_usage,
				   ic_print_usage);
   /* No needs, no core events to track */
}

VG_DETERMINE_INTERFACE_VERSION(ic_pre_clo_init)
