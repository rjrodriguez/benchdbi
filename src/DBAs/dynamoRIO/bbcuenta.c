/* **********************************************************
 * Copyright (c) 2008 VMware, Inc.  All rights reserved.
 * **********************************************************/

/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * * Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 * 
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * 
 * * Neither the name of VMware, Inc. nor the names of its contributors may be
 *   used to endorse or promote products derived from this software without
 *   specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL VMWARE, INC. OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */

/* Instrumentacion por bloques basicos */

#include "dr_api.h"

#define DISPLAY_STRING(msg) dr_printf("%s\n", msg);
#define DISPLAY_STRING_ERR(msg) dr_fprintf(STDERR,"%s\n", msg);
#define NULL_TERMINATE(buf) buf[(sizeof(buf)/sizeof(buf[0])) - 1] = '\0'

// Contador de bloques ejecutados 
static uint64 bcuenta=0; 

// Funcion que se a�adira
static void contar(void) { bcuenta++; } 


static void event_exit(void);
static dr_emit_flags_t event_basic_block(void *drcontext,

void *tag, instrlist_t *bb, bool for_trace, bool translating);
DR_EXPORT void

dr_init(client_id_t id)
{
	dr_register_exit_event(event_exit);
	dr_register_bb_event(event_basic_block);
	dr_log(NULL, LOG_ALL, 1, "Inicializando cliente bbcuenta.\n");
}

// Funcion de salida
static void event_exit(void)
{
	char msg[512];
	int len;
	dr_fprintf(STDERR, "Bloques basicos: %llu\n", bcuenta);
	/*DR_ASSERT(len > 0);
	NULL_TERMINATE(msg);
	DISPLAY_STRING_ERR(msg); */
}

// Funcion llamada por DynamoRio antes de ejecutar un bloque
static dr_emit_flags_t
event_basic_block(void *drcontext, void *tag, instrlist_t *bb,
bool for_trace, bool translating)
{
	// Inserta una llamada a la funcion incrementadora antes de cada bloque
	dr_insert_clean_call(drcontext, bb, instrlist_first(bb),
		(void *)contar, false, 0 );
	return DR_EMIT_DEFAULT;
}
