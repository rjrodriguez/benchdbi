/*BEGIN_LEGAL 
 * Intel Open Source License 
 *
 * Copyright (c) 2002-2013 Intel Corporation. All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are
 *  met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.  Redistributions
 *  in binary form must reproduce the above copyright notice, this list of
 *  conditions and the following disclaimer in the documentation and/or
 *  other materials provided with the distribution.  Neither the name of
 *  the Intel Corporation nor the names of its contributors may be used to
 *  endorse or promote products derived from this software without
 *  specific prior written permission.
 *   
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *   ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE INTEL OR
 *   ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *   END_LEGAL */
#include <iostream>
#include <fstream>
#include "pin.H"

/* Instrumentacion por instrucciones */

// Contador de instrucciones ejecutadas
static UINT64 icont = 0;


// Funcion que se añadira
VOID icuenta() { icont++; }

    
// Funcion llamada por pin antes de ejecutar una instruccion
VOID Instruction(INS ins, VOID *v)
{
    // Inserta una llamada a la funcion incrementadora antes de cada instruccion
    INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)icuenta, IARG_END);
}

KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE, "pintool",
    "o", "icuenta.out", "specify output file name");

// Funcion de salida
VOID Fini(INT32 code, VOID *v)
{
    // Escritura en fichero por si el programa ha cerrado stderr o stdout
    ofstream OutFile;
    OutFile.open(KnobOutputFile.Value().c_str());
    OutFile.setf(ios::showbase);
    OutFile << "-Instrucciones- " << icont << endl;
    OutFile.close();
}


/* ===================================================================== */
/* Main                                                                  */
/* ===================================================================== */
/* argc, argv son la linea entera de comandos: pin -t <toolname> -- ...  */
/* ===================================================================== */

int main(int argc, char * argv[])
{
    // Inicializacion de pin
    PIN_Init(argc, argv);

    // Registro la funcion instruction para ser llamada antes de cualquier instruccion
    INS_AddInstrumentFunction(Instruction, 0);

    // Registro de la funcion de salida
    PIN_AddFiniFunction(Fini, 0);
    
    // Inicio del programa
    PIN_StartProgram();
    
    return 0;
}
